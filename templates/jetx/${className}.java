#define(DBTable table)
package ${basePkg}.model;

import java.util.*;
import com.jfinal.plugin.activerecord.*;

${include("/copyright_class.include")}
@SuppressWarnings("serial")
public class ${className} extends Model<${className}> {

	public static final String TABLE_NAME = "${table.sqlName}";
	public static final String PRIMARY_KEY = "${table.primaryKey}";

	#for (column : table.columns)
	// "${column.sqlName}" -> ${column.remarks}
	#end

	public static final ${className} dao = new ${className}();

	#for (column : table.columns)
	public ${column.javaType} get${column.methodName}(){
		return get("${column.sqlName}");
	}
	public ${className} set${column.methodName}(${column.javaType} ${column.paraName}){
		return set("${column.sqlName}", ${column.paraName});
	}

	#end
}