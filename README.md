#jfinal-rapid

修改`rapid.properties`

用来生成数据字典和model，model生成`get、set`，方便`save，update`~

数据字典的格式`http://team.oschina.net/`的`Markdown`表格~


## 备注
mysql和java类型转换存在部分问题，备注mysql官方的文章，闲时将其改善~
[connector-j-reference-type-conversions](http://dev.mysql.com/doc/connector-j/en/connector-j-reference-type-conversions.html)

## 捐助共勉
<img src="http://soft.dreamlu.net/weixin-9.jpg" width = "200" alt="微信捐助" align=center />
<img src="http://soft.dreamlu.net/weixin-19.jpg" width = "200" alt="微信捐助" align=center />
<img src="http://soft.dreamlu.net/alipay.png" width = "200" alt="支付宝捐助" align=center />

<img src="http://soft.dreamlu.net/qq-9.jpg" width = "200" alt="QQ捐助" align=center />
<img src="http://soft.dreamlu.net/qq-19.jpg" width = "200" alt="QQ捐助" align=center />
