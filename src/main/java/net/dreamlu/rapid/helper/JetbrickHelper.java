package net.dreamlu.rapid.helper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import jetbrick.template.JetEngine;
import jetbrick.template.JetTemplate;

/**
 * jetbrick帮助类
 * @author L.cm
 * email: 596392912@qq.com
 * site:http://www.dreamlu.net
 * @date 2014年9月10日 上午11:46:58
 */
public class JetbrickHelper {

	private static JetEngine engine = null;

	/**
	 * 初始化模板引擎
	 * @param props
	 */
	public JetbrickHelper init(Properties props) {
		// 创建一个默认的 JetEngine
		engine = JetEngine.create(props);
		return this;
	}

	/**
	 * 渲染模板
	 * @param context
	 * @param view
	 * @param fileOut
	 */
	public void render(Map<String, Object> context, String view, String fileOut){
		FileWriter writer = null;
		try {
			// 获取一个模板对象
			JetTemplate template = engine.getTemplate(view);
			File file = new File(fileOut);
			if (!file.exists()) {
				file.createNewFile();
			}
			writer = new FileWriter(new File(fileOut));
			template.render(context, writer);
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(writer);
		}
	}

	/**
	 * 渲染文字模板
	 * @param context
	 * @param view
	 * @param fileOut
	 */
	public String render(Map<String, Object> context, String source){
		// 生成一个模板对象
		JetTemplate template = engine.createTemplate(source);
		// 渲染模板
		StringWriter writer = new StringWriter();
		template.render(context, writer);
		return writer.toString();
	}
}
