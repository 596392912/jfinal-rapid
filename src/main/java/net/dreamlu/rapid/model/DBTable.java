package net.dreamlu.rapid.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.dreamlu.rapid.util.StrUtils;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 对应数据库表
 * @author L.cm
 * email: 596392912@qq.com
 * site:http://www.dreamlu.net
 * @date 2014年9月10日 上午9:41:28
 */
public class DBTable implements java.io.Serializable, Cloneable {

	private static final long serialVersionUID = -4582158020187561010L;

	public final String name;
	public final String remarks;
	private String primaryKey;
	private final List<DBColumn> columns = new ArrayList<DBColumn>();

	public DBTable(String name, String remarks) {
		super();
		this.name = name;
		this.remarks = remarks;
	}

	public List<DBColumn> getColumns() {
		return columns;
	}
	
	public void addColumns(Collection<DBColumn> columns) {
		this.columns.addAll(columns);
	}

	public void addColumn(DBColumn column) {
		this.columns.add(column);
	}

	public String getClassName() {
		return StrUtils.toClassName(this.name);
	}

	public String getClassNameLower() {
		return getClassName().toLowerCase();
	}

	public String getSqlName(){
		return this.name;
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
